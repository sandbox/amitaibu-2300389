api = 2
core = 7.x

projects[ctools] = 1.x-dev

projects[entity][version] = 1.x-dev
projects[entity][patch][] = https://www.drupal.org/files/issues/2264079-entity-wrapper-access-single-entity-reference-2.patch
projects[entity][patch][] = https://www.drupal.org/files/issues/2086225-entity-access-check-node-create-3.patch

projects[entity_validator][version] = 1.x
